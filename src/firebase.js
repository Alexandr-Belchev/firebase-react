import firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyD8FJnKl_0npXsshmjxNwz5XLhTqPF51c4",
  authDomain: "chat-3b223.firebaseapp.com",
  databaseURL: "https://chat-3b223.firebaseio.com",
  projectId: "chat-3b223",
  storageBucket: "",
  messagingSenderId: "333341236550",
  appId: "1:333341236550:web:14c11b6fc5cb6da4"
}

firebase.initializeApp(firebaseConfig)

let database = firebase.database()

firebase.auth().signInAnonymously().catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  // ...
});
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    var isAnonymous = user.isAnonymous;
    console.log(user)
    var uid = user.uid;
    // ...
  } else {
    // User is signed out.
    // ...
  }
  // ...
});
export default database