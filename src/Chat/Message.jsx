import React, {useState} from 'react';
import database from './../firebase'
export default function Message(props) {
  let [message, setMessage] = useState('')
  let sendMessage = () => {
    database.ref('messages').push({sender: 'testnickname', message});
    props.triggerChatUpdate()
  }
  let handleChange = (event) => {
    setMessage(event.target.value);
  }
  return (
    <div>
      <form onSubmit={sendMessage}>
        <textarea placeholder="type..." onChange={handleChange}></textarea>
        <div onClick={sendMessage}>Send mesasge</div>
      </form>
    </div>
  )
}