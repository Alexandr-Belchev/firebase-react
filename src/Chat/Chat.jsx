import React, {useState} from 'react';
import Message from './Message'
import Content from './Content'
export default function App() {
  let [counter, setCounter] = useState(0)
  let testTrigger = () => {
    setCounter(counter+=1)
  }
  return (
    <div>
      <Message triggerChatUpdate={testTrigger}></Message>
      <Content updateContent={counter}></Content>
    </div>    
  )
}