import React, {useEffect, useState} from 'react';
import database from './../firebase'
export default function Content(props) {
  let [messages, setMessages] = useState([{text: '', id: ''}])
  useEffect(() => {
    let items = []
    const messagesRef = database.ref('messages')
      .orderByKey()
      .limitToLast(100);
    messagesRef.on('value', snapshot => {
      const obj = snapshot.val()
      console.log(props.updateContent)
      for(let objKey in obj) {
        items.push(obj[objKey])
      }
      console.log(items)
      console.log(messages)
      setMessages(items)
    });
  }, [props.updateContent])
  return (
    <div>
      {messages.map((message, i) => 
        <div key={i}>
          <p>----------------------</p>
          <p>{message.message}</p>
          <p>{message.sender}</p>
        </div>
      )}
    </div>
  )
}